package org.yunfeng.registration.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class RegistrationCenterBoot {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationCenterBoot.class, args);
	}
}

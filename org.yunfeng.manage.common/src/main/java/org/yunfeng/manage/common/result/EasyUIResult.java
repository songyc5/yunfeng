package org.yunfeng.manage.common.result;

import java.io.Serializable;
import java.util.List;
/**
 * 统一返回码
 * @author songyc5
 */
public class EasyUIResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer total;

    private List<?> rows;

    public EasyUIResult() {}

    public EasyUIResult(Integer total, List<?> rows) {
        this.total = total;
        this.rows = rows;
    }

    public EasyUIResult(Long total, List<?> rows) {
        this.total = total.intValue();
        this.rows = rows;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }

    /**
     * Object是集合转化
     * 
     * @param jsonData json数据
     * @param clazz 集合中的类型
     * @return
     */
    public static EasyUIResult formatToList(String jsonData, Class<?> clazz) {
       //TODO 利用fastJson.API重写此方法
        return null;
    }

}

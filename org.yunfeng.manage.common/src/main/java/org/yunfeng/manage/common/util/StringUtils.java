package org.yunfeng.manage.common.util;

import java.util.Arrays;

public class StringUtils {
	
	private StringUtils() {}
	
	public static String arrToString(String[] arr) {
		String arrStr = Arrays.toString(arr);
		String result = arrStr.substring(1, arrStr.length() - 1);
		return result;
	}
}

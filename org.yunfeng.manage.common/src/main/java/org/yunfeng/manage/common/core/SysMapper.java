package org.yunfeng.manage.common.core;


import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.ExampleMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Marker;
import tk.mybatis.mapper.common.MySqlMapper;


/**
 * 自定义通用Mapper接口,适配MySql
 * 结合泛型继承后,单表查询可动态生成Sql,对应的mapper.xml留空即可,多表操作不支持 
 */
public interface SysMapper<T> extends
	BaseMapper<T>,
	MySqlMapper<T>,
	IdsMapper<T>,
	ExampleMapper<T>,Marker{
	
}

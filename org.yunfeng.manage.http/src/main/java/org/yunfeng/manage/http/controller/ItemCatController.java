package org.yunfeng.manage.http.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yunfeng.manage.base.api.facade.ItemCatServiceFacade;
import org.yunfeng.manage.base.api.pojo.ItemCat;

@RestController
@RequestMapping("/item/cat")
public class ItemCatController {

	@Resource
	private ItemCatServiceFacade itemCatServiceFacade;
	
	@RequestMapping("/itemCatAll")
	public List<ItemCat> queryAll(){
		return itemCatServiceFacade.queryAll();
	}
	
	@PostMapping(value="list")
	public List<ItemCat> queryList(@RequestParam(defaultValue="0") String id){
		 return itemCatServiceFacade.queryById(id);
	}
}

package org.yunfeng.manage.http.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//统一中转到veiws下的jsp页面
@Controller
public class PageController {
	
	@GetMapping("/page/{pageName}") //利用RESTFul形式,最后一个占位符就是页面名称
	public String homePage(@PathVariable String pageName){
		return pageName;
	}
}

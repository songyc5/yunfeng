package org.yunfeng.manage.http.oss;
/**
 * 图片信息实体类
 * 用于分析图片是否病毒
 */
public class ImageDetails {
	private boolean virus;
	private String height;
	private String width;
	
	public boolean isVirus() {
		return virus;
	}
	public void setVirus(boolean virus) {
		this.virus = virus;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
}
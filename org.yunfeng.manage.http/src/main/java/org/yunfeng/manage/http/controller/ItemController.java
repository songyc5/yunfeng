package org.yunfeng.manage.http.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yunfeng.manage.base.api.facade.ItemServiceFacade;
import org.yunfeng.manage.base.api.pojo.Item;
import org.yunfeng.manage.common.result.EasyUIResult;
import org.yunfeng.manage.common.result.SysResult;

@RestController
@RequestMapping("/item")
public class ItemController {

	@Resource
	private ItemServiceFacade itemServiceFacade;

	// 商品列表页面，/item/query EasyUI.datagrid组件要求：page当前页，rows每页条数
	@GetMapping(value = "query", params = "page, rows")
	public EasyUIResult queryItemList(Integer page, Integer rows) {
		return itemServiceFacade.queryItemList(page, rows);
	}

	@PostMapping("delete")
	public SysResult deleteItemByIds(String ids) {
		try {
			itemServiceFacade.deleteByIds(ids);
			return SysResult.ok();
		} catch (Exception e) {
			return SysResult.build(201, e.getMessage());
		}
	}

	@PostMapping("save")
	public SysResult saveItem(Item item, String desc) {
		try {
			itemServiceFacade.saveItem(item, desc);
			return SysResult.ok();
		} catch (Exception e) {
			return SysResult.build(201, e.getMessage());
		}
	}

	@PostMapping("update")
	public SysResult updateItem(Item item) {
		try {
			itemServiceFacade.updateItem(item);
			return SysResult.ok();
		} catch (Exception e) {
			return SysResult.build(201, e.getMessage());
		}
	}

	@PostMapping("reshelf")
	public SysResult reshelfItem(long[] ids) {
		try {
			itemServiceFacade.reshelfItem(ids);
			return SysResult.ok();
		} catch (Exception e) {
			return SysResult.build(201, e.getMessage());
		}
	}

	@PostMapping("instock")
	public SysResult instockItem(long[] ids) {
		try {
			itemServiceFacade.instockItem(ids);
			return SysResult.ok();
		} catch (Exception e) {
			return SysResult.build(201, e.getMessage());
		}
	}

}

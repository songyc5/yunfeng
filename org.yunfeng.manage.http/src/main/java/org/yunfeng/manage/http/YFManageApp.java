package org.yunfeng.manage.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter4;

/**
 * Springboot-Dubbo-Consumer启动类
 * @author songyc5
 */
@SpringBootApplication
@ImportResource("classpath:META-INF/dubbo/*.xml")
public class YFManageApp extends SpringBootServletInitializer{
	private final static Logger logger = LoggerFactory.getLogger(YFManageApp.class);
	
	public static void main(String[] args) {
		SpringApplication.run(YFManageApp.class, args);
		logger.info(YFManageApp.class.getSimpleName() + " is started!");
	}
	
	//集成jsp
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(YFManageApp.class);
	}
	
	//集成fastjson
	@Bean
	public HttpMessageConverters FastJsonHttpMessageConverter4s(){
		FastJsonHttpMessageConverter4 converter = new FastJsonHttpMessageConverter4();
		FastJsonConfig config  = new FastJsonConfig();
		config.setSerializerFeatures(
				SerializerFeature.PrettyFormat,
				SerializerFeature.WriteDateUseDateFormat//改日期格式为 yyyy-MM-dd HH:mm:ss
				);
		converter.setFastJsonConfig(config);
		
		return new HttpMessageConverters(converter);
	}
}

package org.yunfeng.manage.base.api.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * 为了通用Mapper加上JPA，描述java持久类和数据库表映射关系
 * JPA映射：4个注解就够了
 * 1）类和表的映射
 * 2）属性和数据库表的字段映射
 * 3）标识表的主键
 * 4）标识主键自增
 * 
 * select parent_id,name from tb_item_cat
 */

@Table(name="tb_item_cat")
public class ItemCat implements Serializable{
	
	private static final long serialVersionUID = 4243389080285946127L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long parentId;
	private String name;
	private Integer status;
	private Integer sortOrder;
	private Boolean isParent;
	private Date created;
	private Date updated;
	
	public Long getParentId() {
		return parentId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}
	//利用json根据get方法创建对象属性的原理,给easyUI树准备的text属性
	public String getText(){
		return name;
	}
	//利用json根据get方法创建对象属性的原理,给easyUI树准备的state属性
	public String getState(){
		return this.getIsParent()?"closed" : "open";
	}
	@Override
	public String toString() {
		return "ItemCat [id=" + id + ", parentId=" + parentId + ", name="
				+ name + ", status=" + status + ", sortOrder=" + sortOrder
				+ ", isParent=" + isParent + ", created=" + created
				+ ", updated=" + updated + "]";
	}
}

package org.yunfeng.manage.base.api.facade;

import java.util.List;

import org.yunfeng.manage.base.api.pojo.ItemCat;

public interface ItemCatServiceFacade {
	/**
	 * 以EasyUI节点id作为parent_id 查找所有子品类
	 * @param id 商品节点id
	 * @return 返回此商品类目下,所有商品类
	 */
	List<ItemCat> queryById(String id);

	List<ItemCat> queryAll();

}

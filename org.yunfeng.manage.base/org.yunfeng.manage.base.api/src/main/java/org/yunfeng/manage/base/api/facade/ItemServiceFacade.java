package org.yunfeng.manage.base.api.facade;

import org.yunfeng.manage.base.api.pojo.Item;
import org.yunfeng.manage.base.api.pojo.ItemDesc;
import org.yunfeng.manage.common.result.EasyUIResult;

public interface ItemServiceFacade {
	/** 分页查询*/
	EasyUIResult queryItemList(Integer page, Integer rows);

	/** 新增*/
	void saveItem(Item item, String desc);

	/** 修改*/
	void updateItem(Item item);

	/** 删除*/
	void deleteByIds(String ids);

	/** 查找商品描述信息*/
	ItemDesc queryDescById(Long id);

	/** 商品上架 */
	void reshelfItem(long[] ids);

	/** 商品下架*/
	void instockItem(long[] ids);
}

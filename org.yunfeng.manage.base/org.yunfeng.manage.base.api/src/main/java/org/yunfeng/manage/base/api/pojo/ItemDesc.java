package org.yunfeng.manage.base.api.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="tb_item_desc")
public class ItemDesc implements Serializable{
	
	private static final long serialVersionUID = 3125449441940814346L;
	
	@Id
	private Long itemId; //非自增
	private String itemDesc;
	private Date updated;
	private Date created;
	
	public Long getItemId() {
		return itemId;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	
}

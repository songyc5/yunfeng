package org.yunfeng.manage.base.core.mapper;

import org.yunfeng.manage.base.api.pojo.ItemDesc;
import org.yunfeng.manage.common.core.SysMapper;

public interface ItemDescMapper extends SysMapper<ItemDesc>{

}

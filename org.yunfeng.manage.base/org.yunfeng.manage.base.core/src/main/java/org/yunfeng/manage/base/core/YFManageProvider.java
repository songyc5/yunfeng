package org.yunfeng.manage.base.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.yunfeng.manage.common.core.SysMapper;

/**
 * springboot-dubbo-provider启动类, 集成通用mapper3
 * 
 * @author songyc5
 */
@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "org.yunfeng.manage.base.core.mapper", markerInterface = SysMapper.class)
@ImportResource({ "classpath:META-INF/dubbo/*.xml" })
public class YFManageProvider {

	private static final Logger logger = LoggerFactory
			.getLogger(YFManageProvider.class);

	@Bean
	public CountDownLatch closeLatch() {
		return new CountDownLatch(1);
	}

	public static void main(String[] args) throws IOException {
		ApplicationContext context = new SpringApplicationBuilder()
				.sources(YFManageProvider.class).web(false) // 禁止boot启动web环境
				.run(args);
		logger.info(YFManageProvider.class.getSimpleName() + " is started!");

		CountDownLatch closeLatch = context.getBean(CountDownLatch.class);
		try {
			closeLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.in.read();
		}
	}

}

@Component
class MyBean {
	private boolean debug;
	private ApplicationArguments args;
	private Set<String> optionNames;
	private List<String> nonOptionfiles = new ArrayList<String>();
	private String[] sourceArgs;

	@Autowired
	public MyBean(ApplicationArguments args) {
		optionNames = args.getOptionNames();
		debug = args.containsOption("debug");
		nonOptionfiles = args.getNonOptionArgs();
		sourceArgs = args.getSourceArgs();
		this.args = args;
	}

	public boolean isDebug() {
		return debug;
	}

	public ApplicationArguments getArgs() {
		return args;
	}

	public void setArgs(ApplicationArguments args) {
		this.args = args;
	}

	public Set<String> getOptionNames() {
		return optionNames;
	}

	public void setOptionNames(Set<String> optionNames) {
		this.optionNames = optionNames;
	}

	public String[] getSourceArgs() {
		return sourceArgs;
	}

	public void setSourceArgs(String[] sourceArgs) {
		this.sourceArgs = sourceArgs;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public List<String> getNonOptionfiles() {
		return nonOptionfiles;
	}

	public void setNonOptionfiles(List<String> nonOptionfiles) {
		this.nonOptionfiles = nonOptionfiles;
	}

}

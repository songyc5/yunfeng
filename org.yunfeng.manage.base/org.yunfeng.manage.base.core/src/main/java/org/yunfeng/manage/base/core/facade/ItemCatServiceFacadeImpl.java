package org.yunfeng.manage.base.core.facade;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yunfeng.manage.base.api.facade.ItemCatServiceFacade;
import org.yunfeng.manage.base.api.pojo.ItemCat;
import org.yunfeng.manage.base.core.mapper.ItemCatMapper;

@Transactional
@Service("itemCatServiceFacade")
public class ItemCatServiceFacadeImpl implements ItemCatServiceFacade {

	@Resource
	private ItemCatMapper itemCatMapper;

	/**
	 * 以EasyUI节点id作为parent_id 查找所有子品类
	 */
	@Override
	public List<ItemCat> queryById(String id) {
		return itemCatMapper.queryById(id);
	}

	@Override
	public List<ItemCat> queryAll() {
		return itemCatMapper.selectAll();
	}
}

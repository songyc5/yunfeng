package org.yunfeng.manage.base.core.mapper;

import java.util.List;

import org.yunfeng.manage.base.api.pojo.ItemCat;
import org.yunfeng.manage.common.core.SysMapper;

public interface ItemCatMapper extends SysMapper<ItemCat>{

	List<ItemCat> queryById(String id);

}

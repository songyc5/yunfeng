package org.yunfeng.manage.base.core.mapper;

import java.util.List;

import org.yunfeng.manage.base.api.pojo.Item;
import org.yunfeng.manage.common.core.SysMapper;

public interface ItemMapper extends SysMapper<Item>{

	List<Item> queryItemList();

}

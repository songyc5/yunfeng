# 云锋后台

# 技术选型:
jdk 1.8
Springboot 1.51 - 继承starter-parent版本依赖
Dubbo 2.53 
Zookeeper 3.46
zkclient 0.8
HikariCP 2.6.0
通用Mapper3 3.39
logback 1.20

# 启动方法:
1.解压启动 zookeeper
2.运行 com.yunfeng.manage.base.core.YFManageProvider.java
3.运行 com.yunfeng.manage.web.YFManageApp.java

# 更新日志:
### 2017.2.9 Springboot + Dubbo集成
### 2017.2.9 更新HikariCP版本为2.6.0
### 2017.2.13 整合后台页面资源, 取消devtools配置
### 2017.2.15 移动静态资源到classpath:static/, 调整fastJson转换日期格式
### 2017.2.16 增加页面favicon 添加商品上下架接口
### 2017.2.17 优化HikariCP配置



